import logging
from os import path, listdir

import numpy as np
from pandas import read_csv


class PlotROIRepresentations(object):

    def __init__(self):
        self.hidden_feat_dir = "./output/hidden_feats"

    def get_indices(self):
        return range(int(len(listdir(self.hidden_feat_dir))/3))

    def read_files(self):
        logging.info(f"Reading files from {self.hidden_feat_dir}")
        data = {}
        for index in self.get_indices():
            feats = np.load(path.join(self.hidden_feat_dir,
                                     f"feats_{str(index).zfill(4)}.npy"))
            probs = np.load(path.join(self.hidden_feat_dir,
                                     f"probs_{str(index).zfill(4)}.npy"))
            rois = np.load(path.join(self.hidden_feat_dir,
                                     f"rois_{str(index).zfill(4)}.npy"))
            data[int(index)] = {"feats": feats,
                                "probs": probs,
                                "rois": rois}
        return data

    def get_mot_acc(self):
        return read_csv("/home/jake/data/tracker_acc.csv")

    def main(self):
        data = self.read_files()
        dets = read_csv("/home/jake/src/sort/output/"+\
                        "test1_1-230902_dets.csv.txt",
                        names=["frame", "obj_id", "x", "y", "w", "h", "conf",
                               "3dx", "3dy", "3dz"])
        from ipdb import set_trace; set_trace()
        pass

if __name__ == "__main__":
    import coloredlogs
    coloredlogs.install(level="INFO",
                        fmt="%(asctime)s %(levelname)s %(module)s" + \
                            "- %(funcName)s: %(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")
    plt_roi = PlotROIRepresentations()
    plt_roi.main()
