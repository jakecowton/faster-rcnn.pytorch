from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import _init_paths
import os
from os import listdir, path
import sys
import numpy as np
import argparse
import pprint
import pdb
import time
import cv2
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim

import torchvision.transforms as transforms
import torchvision.datasets as dset
from scipy.misc import imread
from roi_data_layer.roidb import combined_roidb
from roi_data_layer.roibatchLoader import roibatchLoader
from model.utils.config import cfg, cfg_from_file, cfg_from_list, get_output_dir
from model.rpn.bbox_transform import clip_boxes
from model.nms.nms_wrapper import nms
from model.rpn.bbox_transform import bbox_transform_inv
from model.utils.net_utils import save_net, load_net, vis_detections
from model.utils.blob import im_list_to_blob
from model.faster_rcnn.vgg16 import vgg16
from model.faster_rcnn.resnet import resnet
import pdb

from tqdm import tqdm


class ObjectDetector(object):

    def __init__(self, net, classes, weight_file, class_agnostic=False):
        self.net = net
        self.classes = classes
        self.weight_file = weight_file
        self.class_agnostic = class_agnostic
        self.img_root = "/home/jake/pig_rgb/1_1/"

    def reconfigure_model(self, funnel=True):
        bbox_pred = 4 if self.class_agnostic is True \
                      else len(self.classes)*4

        if funnel is False:
            logging.warning(f"Modifying class labels layer")
            self.detector.RCNN_cls_score = nn.Linear(2048,
                                                       len(self.classes))

            logging.warning(f"Modifying class bboxes layer")
            self.detector.RCNN_bbox_pred = nn.Linear(2048, bbox_pred)
        else:
            logging.warning(f"Modifying class labels layer")
            self.detector.RCNN_cls_score = nn.Sequential(
                nn.Linear(2048, 21),
                nn.Linear(21, len(self.classes))
                )

            logging.warning(f"Modifying class bboxes layer")
            self.detector.RCNN_bbox_pred = nn.Sequential(
                nn.Linear(2048, 21*4),
                nn.Linear(21*4, 8)
                )

    def load_model(self):
        if self.net == 'vgg16':
            fasterRCNN = vgg16(self.classes, pretrained=False,
                               class_agnostic=self.class_agnostic)
        elif self.net == 'res101':
            fasterRCNN = resnet(self.classes, 101, pretrained=False,
                               class_agnostic=self.class_agnostic)
        elif self.net == 'res50':
            fasterRCNN = resnet(self.classes, 50, pretrained=False,
                               class_agnostic=self.class_agnostic)
        elif self.net == 'res152':
            fasterRCNN = resnet(self.classes, 152, pretrained=False,
                               class_agnostic=self.class_agnostic)
        else:
            raise ValueError("network is not defined")

        fasterRCNN.create_architecture()
        self.detector = fasterRCNN
        self.reconfigure_model(False)

        logging.debug(f"Loading {self.weight_file}")
        checkpoint = torch.load(self.weight_file)

        fasterRCNN.load_state_dict(checkpoint['model'])
        if 'pooling_mode' in checkpoint.keys():
            cfg.POOLING_MODE = checkpoint['pooling_mode']

        self.detector = fasterRCNN
        self.detector.cuda()
        fasterRCNN.eval()
        logging.debug("Detector created")

    def initialise_tensors(self):
        logging.debug("Initialising tensors")
        with torch.no_grad():
            im_data = Variable(torch.FloatTensor(1).cuda())
            im_info = Variable(torch.FloatTensor(1).cuda())
            num_boxes = Variable(torch.LongTensor(1).cuda())
            gt_boxes = Variable(torch.FloatTensor(1).cuda())
        return im_data, im_info, num_boxes, gt_boxes

    def _get_image_blob(self, im):
        """Converts an image into a network input.
        Arguments:
          im (ndarray): a color image in BGR order
        Returns:
          blob (ndarray): a data blob holding an image pyramid
          im_scale_factors (list): list of image scales (relative to im) used
            in the image pyramid
        """
        im_orig = im.astype(np.float32, copy=True)
        im_orig -= cfg.PIXEL_MEANS

        im_shape = im_orig.shape
        im_size_min = np.min(im_shape[0:2])
        im_size_max = np.max(im_shape[0:2])

        processed_ims = []
        im_scale_factors = []

        for target_size in cfg.TEST.SCALES:
            im_scale = float(target_size) / float(im_size_min)
            # Prevent the biggest axis from being more than MAX_SIZE
            if np.round(im_scale * im_size_max) > cfg.TEST.MAX_SIZE:
                im_scale = float(cfg.TEST.MAX_SIZE) / float(im_size_max)
            im = cv2.resize(im_orig, None, None, fx=im_scale, fy=im_scale,
                    interpolation=cv2.INTER_LINEAR)
            im_scale_factors.append(im_scale)
            processed_ims.append(im)

        # Create a blob to hold the input images
        blob = im_list_to_blob(processed_ims)

        return blob, np.array(im_scale_factors)

    def generate_image_list(self):
        img_list = (path.join(self.img_root, d, f) \
                    for d in sorted(listdir(self.img_root)) \
                   for f in sorted(listdir(path.join(self.img_root, d))))
        return img_list

    def iterate_images(self, vis=False):
        if vis is True:
            with open("logs/mass_detection.csv", "a") as f:
                f.write("file,x,y,max_x,max_y,conf\n")

        self.load_model()

        im_data, im_info, num_boxes, gt_boxes = self.initialise_tensors()

        logging.debug("Generating image list")
        img_list = self.generate_image_list()
        no_images = sum(1 for x in self.generate_image_list())
        count = 0

        for img in tqdm(img_list):
            total_tic = time.time()

            im_file = img
            im_in = np.array(imread(im_file))
            if len(im_in.shape) == 2:
                im_in = im_in[:,:,np.newaxis]
                im_in = np.concatenate((im_in,im_in,im_in), axis=2)
            # rgb -> bgr
            try:
                im = im_in[:,:,::-1]
            except:
                continue

            blobs, im_scales = self._get_image_blob(im)
            assert len(im_scales) == 1, "Only single-image batch implemented"
            im_blob = blobs
            im_info_np = np.array([[im_blob.shape[1], im_blob.shape[2],
                                    im_scales[0]]], dtype=np.float32)

            im_data_pt = torch.from_numpy(im_blob)
            im_data_pt = im_data_pt.permute(0, 3, 1, 2)
            im_info_pt = torch.from_numpy(im_info_np)

            im_data.data.resize_(im_data_pt.size()).copy_(im_data_pt)
            im_info.data.resize_(im_info_pt.size()).copy_(im_info_pt)
            gt_boxes.data.resize_(1, 1, 5).zero_()
            num_boxes.data.resize_(1).zero_()

            det_tic = time.time()

            rois, cls_prob, bbox_pred, \
            rpn_loss_cls, rpn_loss_box, \
            RCNN_loss_cls, RCNN_loss_bbox, \
            rois_label = self.detector(im_data, im_info, gt_boxes, num_boxes)

            scores = cls_prob.data
            boxes = rois.data[:, :, 1:5]

            if cfg.TEST.BBOX_REG:
                box_deltas = bbox_pred.data
                if cfg.TRAIN.BBOX_NORMALIZE_TARGETS_PRECOMPUTED:
                # Optionally normalize targets by a precomputed mean and stdev
                    box_deltas = box_deltas.view(-1, 4) * \
                        torch.FloatTensor(cfg.TRAIN.BBOX_NORMALIZE_STDS)\
                                         .cuda() +\
                        torch.FloatTensor(cfg.TRAIN.BBOX_NORMALIZE_MEANS)\
                                         .cuda()
                    box_deltas = box_deltas.view(1, -1, 4 * len(self.classes))

                pred_boxes = bbox_transform_inv(boxes, box_deltas, 1)
                pred_boxes = clip_boxes(pred_boxes, im_info.data, 1)
            else:
                # Simply repeat the boxes, once for each class
                pred_boxes = np.tile(boxes, (1, scores.shape[1]))

            pred_boxes /= im_scales[0]

            scores = scores.squeeze()
            pred_boxes = pred_boxes.squeeze()
            det_toc = time.time()
            detect_time = det_toc - det_tic
            misc_tic = time.time()
            im2show = np.copy(im)
            thresh = 0.05
            for j in range(1, len(self.classes)):
                inds = torch.nonzero(scores[:,j]>thresh).view(-1)
                # if there is det
                if inds.numel() > 0:
                    cls_scores = scores[:,j][inds]
                    _, order = torch.sort(cls_scores, 0, True)
                    cls_boxes = pred_boxes[inds][:, j * 4:(j + 1) * 4]

                    cls_dets = torch.cat((cls_boxes, cls_scores.unsqueeze(1)),
                                         1)
                    cls_dets = cls_dets[order]
                    keep = nms(cls_dets, cfg.TEST.NMS,
                               force_cpu=not cfg.USE_GPU_NMS)
                    cls_dets = cls_dets[keep.view(-1).long()]
                    im2show = vis_detections(im2show,
                                             self.classes[j],
                                             cls_dets.cpu().numpy(),
                                             0.5)

            misc_toc = time.time()
            nms_time = misc_toc - misc_tic

            if vis is True:
                result_path = os.path.join(img[:-4] + "_det.jpg")
                cv2.imwrite(result_path, im2show)
            else:
                with open("logs/mass_detection.csv", "a") as f:
                    try:
                        for x,y,w,h,c in cls_dets.cpu().numpy():
                            val = ",".join([img, str(x), str(y), str(w), str(h),
                                              str(c)])
                            f.write(val+"\n")
                    except UnboundLocalError:
                        pass

            sys.stdout.write(f" | im_detect: {count}/{no_images} "+\
                             f"{round(detect_time, 3)}s "+\
                             f"{round(nms_time, 3)}s \r")
            sys.stdout.flush()
            count += 1


if __name__ == "__main__":
    import coloredlogs
    coloredlogs.install(level="DEBUG",
                        fmt="%(asctime)s %(levelname)s %(module)s" + \
                            "- %(funcName)s: %(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")
    net = "res101"
    classes = ("__background__", "pig")
    weight_file = "./models/res101/pigs_voc/faster_rcnn_1810251_30_121.pth"

    detector = ObjectDetector(net, classes, weight_file)
    detector.iterate_images(vis=False)
