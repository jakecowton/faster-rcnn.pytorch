from os import path
from xml.etree import ElementTree as ET
from math import sqrt

from PIL import Image, ImageStat
import numpy as np
import matplotlib as mp
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
font = {'size'   : 26}
plt.rc('font', **font)

ROOT_DIR = "/home/jake/pig_voc"


class Frame(object):

    def __init__(self, frame):
        self.id = frame
        self.img_path = path.join(ROOT_DIR, "jpg_images", f"{frame}.jpg")
        self.ann_path = path.join(ROOT_DIR, "annotations", f"{frame}.xml")

    @property
    def ann(self):
        return ET.parse(self.ann_path)

    @property
    def img(self):
        return self._load_image()

    @property
    def number_of_pigs(self):
        return len(self._get_bbox_objects())

    @property
    def has_densely_packed(self):
        if self.count_overlaps() >= 4:
            return True
        else:
            return False

    def count_overlaps(self):
        overlaps = []
        for i, bbox1 in enumerate(self.bboxes):
            for j, bbox2 in enumerate(self.bboxes):
                if i == j:
                    continue
                iou = self.iou(bbox1, bbox2)
                if iou > 0:
                    overlaps.append(iou)
        return len(set(overlaps))

    @property
    def brightness(self):
        try:
            self.avg_brightness
        except AttributeError:
            bbox_brightnesses = []
            for bbox in self.bboxes:
                bbox_img = self._get_bbox_img(bbox)
                stat = ImageStat.Stat(bbox_img)
                r,g,b = stat.mean
                brightness = sqrt(0.241*(r**2) + 0.691*(g**2) + 0.068*(b**2))
                bbox_brightnesses.append(brightness)

            self.avg_brightness = np.mean(bbox_brightnesses)

        return self.avg_brightness

    @property
    def bboxes(self):
        bboxes = []
        for ix, obj in enumerate(self._get_bbox_objects()):
            bbox = obj.find('bndbox')
            # Make pixel indexes 0-based
            x1 = float(bbox.find('xmin').text) - 1
            y1 = float(bbox.find('ymin').text) - 1
            x2 = float(bbox.find('xmax').text) - 1
            y2 = float(bbox.find('ymax').text) - 1

            bboxes.append({"x1": x1, "x2": x2, "y1": y1, "y2":y2})
        return bboxes

    def _get_bbox_img(self, bbox):
        return self.img.crop((bbox["x1"], bbox["y1"],
                              bbox["x2"], bbox["y2"]))

    def _load_image(self):
        return Image.open(self.img_path)

    def _get_bbox_objects(self):
        return self.ann.findall("object")

    def iou(self, bb1, bb2):
        assert bb1['x1'] < bb1['x2']
        assert bb1['y1'] < bb1['y2']
        assert bb2['x1'] < bb2['x2']
        assert bb2['y1'] < bb2['y2']

        x_left = max(bb1['x1'], bb2['x1'])
        y_top = max(bb1['y1'], bb2['y1'])
        x_right = min(bb1['x2'], bb2['x2'])
        y_bottom = min(bb1['y2'], bb2['y2'])

        if x_right < x_left or y_bottom < y_top:
            return 0.0

        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
        bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

        iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
        assert iou >= 0.0
        assert iou <= 1.0
        return iou

def plot_data(test_list):
    fig = plt.figure(figsize=(20,10))

    ax = fig.add_subplot(2,2,1)
    labels, counts = np.unique([x.count_overlaps() for x in test_list],
                               return_counts=True)
    ax.bar(labels, counts, align="center")
    ax.set_xlabel("Number of overlaps")
    ax.set_ylabel("Number of images")
    ax.set_xlim([-1,14])
    ax.set_xticks(range(15))

    ax = fig.add_subplot(2,2,2)
    labels, counts = np.unique([x.number_of_pigs for x in test_list],
                               return_counts=True)
    ax.bar(labels, counts, align="center")
    ax.set_xlabel("Number of pigs in image")
    ax.set_ylabel("Number of images")
    ax.set_xticks(range(15))

    ax = fig.add_subplot(2,2,3)
    brightnesses = np.load("brightnesses.npy", allow_pickle=True)
    ax.hist(brightnesses, bins=255)
    bins_labels(ax, range(255))
    ax.set_xlabel("Average pig brightness")
    ax.set_ylabel("Number of images")

    plt.tight_layout()
    plt.savefig("/home/jake/test_segment_plots.png")
    plt.close()
    # plt.show()

def bins_labels(ax, bins, **kwargs):
    bin_w = (max(bins) - min(bins)) / (len(bins) - 1)
    ax.set_xticks(np.arange(min(bins)+bin_w/2, max(bins), bin_w), bins, **kwargs)
    ax.set_xlim(bins[0], bins[-1])

if __name__ == "__main__":
    with open(path.join(ROOT_DIR, "test2.txt"), "r") as f:
        test_list = [line.replace("\n", "") for line in f]
    test_list = [Frame(x) for x in test_list]
    plot_data(test_list)

    # dense_images = [x for x in test_list if x.has_densely_packed]
    # with open(path.join(ROOT_DIR, "dense_test2.txt"), "w") as f:
        # f.writelines([frame.id+"\n" for frame in dense_images])
    # many_pigs = [x for x in test_list if x.number_of_pigs > 10]
    # with open(path.join(ROOT_DIR, "many_pigs_test2.txt"), "w") as f:
        # f.writelines([frame.id+"\n" for frame in many_pigs])
    # bright_pigs = [x for x in test_list if x.brightness > 180]
    # with open(path.join(ROOT_DIR, "bright_pigs_test2.txt"), "w") as f:
        # f.writelines([frame.id+"\n" for frame in bright_pigs])
    # dark_pigs = [x for x in test_list if x.brightness <= 100]
    # with open(path.join(ROOT_DIR, "dark_pigs_test2.txt"), "w") as f:
        # f.writelines([frame.id+"\n" for frame in dark_pigs])
    # bright_list = ["221134", "251100", "251145", "261346"]
    # bright_images = []
    # for frame in test_list:
        # for bright_seg in bright_list:
            # if bright_seg in frame.id:
                # bright_images.append(frame)
    # with open(path.join(ROOT_DIR, "bright_pigs_2_test2.txt"), "w") as f:
        # f.writelines([frame.id+"\n" for frame in bright_images])
