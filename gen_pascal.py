from os import path
from pandas import read_csv

from pascal_writer import PascalVocWriter


class GeneratePascalXML(object):

    def __init__(self, mass_detections_file):
        self.mass_dets = read_csv(mass_detections_file,
                                  names=["file", "x", "y", "max_x", "max_y",
                                         "conf"])

    def add_boxes(self, pv, df):
        logging.debug("Adding BBs")
        for i, row in df.iterrows():
            logging.debug(f"Added BB {i+1}")
            pv.addBndBox(round(row.x),
                         round(row.y),
                         round(row.max_x),
                         round(row.max_y),
                         "pig",
                         False)
        return pv

    def iterate_images(self):
        logging.info("Iterating images")
        filenames = [f for f in sorted(self.mass_dets.file.unique())]
        for fn in filenames:
            logging.debug(f"Processing {path.basename(fn)}")
            df = self.mass_dets[self.mass_dets.file == fn]
            voc_writer = PascalVocWriter(path.dirname(fn),
                                         path.basename(fn),
                                         [1920,1080])
            voc_writer = self.add_boxes(voc_writer, df)

            logging.info(f"Saving {path.basename(fn)} to " +\
                         path.join("/home/jake/pig_rgb/1_1/annotations/",
                                   path.basename(fn)[:-4] + ".xml"))

            voc_writer.save(path.join("/home/jake/pig_rgb/1_1/annotations/",
                                      path.basename(fn)[:-4] + ".xml"))
            break


if __name__ == "__main__":
    import logging
    import coloredlogs
    coloredlogs.install(level="DEBUG",
                        fmt="%(asctime)s %(levelname)s %(module)s" + \
                            "- %(funcName)s: %(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")
    gen = GeneratePascalXML("./logs/mass_detection.csv")
    gen.iterate_images()
