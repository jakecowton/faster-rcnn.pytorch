import logging
from datetime import datetime
import socket

import torch
from slackweb import Slack

from trainval_net import Trainer

HOSTNAME = socket.gethostname()


class TrainVariants(object):
    def __init__(self, epochs, bs, nw=1, mGPUs=1):
        self.epochs = epochs
        self.bs = bs
        self.nw = nw
        self.mGPUs = mGPUs
        self.cuda = torch.cuda.is_available()
        self.slack = Slack("https://hooks.slack.com/services/T08KUB6HE/"+\
                           "B6S473F5E/CSL114Uv6pgWlH0rZ7AfPZh8")

    def get_dt(self):
        dt = datetime.now().strftime("%y%m%d")
        return dt

    def inet_pascal_pigs(self, dt):
        """
        Trained on pascal then FC layers modified for pigs
        """
        session = f"{dt}1"
        logging.info(f"[{session}] Training on Pascal VOC with FC reconfigured")
        trained_pascal = Trainer({"dataset": "pascal_voc",
                                  "net": "res101",
                                  "max_epochs": self.epochs,
                                  "disp_interval": 100,
                                  "batch_size": self.bs,
                                  "cuda": True,
                                  "mGPUs": self.mGPUs,
                                  "session": session,
                                  "log_path": f"logs/pascal_voc_{session}",
                                  "is_optimising": True,
                                  "ovthresh": 0.6,
                                 })
        trained_pascal.train()

        logging.info(f"[{session}] Training on Pigs VOC")
        trained_pigs = Trainer({"dataset": "pigs_voc",
                                "net": "res101",
                                "max_epochs": self.epochs * 2,
                                "disp_interval": 100,
                                "batch_size": self.bs,
                                "cuda": True,
                                "mGPUs": self.mGPUs,
                                "session": session,
                                "resume": True,
                                "checksession": session,
                                "checkepoch": self.epochs,
                                "checkpoint": 1251,
                                "resume_dataset": "pascal_voc",
                                "transfer": True,
                                "resume_classes": 21,
                                "log_path": f"logs/pascal_pigs_voc_{session}",
                                "is_optimising": True,
                                # "and_test": True,
                                "ovthresh": 0.6,
                               })
        trained_pigs.train()

    def inet_pascal_pigs_funnel(self, dt):
        """
        Trained on pascal then another layer added to FC
        """
        session = f"{dt}4"
        logging.info(f"[{session}] Training on Pigs VOC")
        trained_pigs_funnel = Trainer({"dataset": "pigs_voc",
                                       "net": "res101",
                                       "max_epochs": self.epochs * 2,
                                       "disp_interval": 100,
                                       "batch_size": self.bs,
                                       "cuda": True,
                                       "mGPUs": self.mGPUs,
                                       "session": session,
                                       "resume": True,
                                       "checksession": f"{dt}1",
                                       "checkepoch": self.epochs,
                                       "checkpoint": 1251,
                                       "resume_dataset": "pascal_voc",
                                       "transfer": True,
                                       "resume_classes": 21,
                                       "log_path": f"logs/pascal_pigs_voc_funnel_{session}",
                                       "is_optimising": True,
                                       # "and_test": True,
                                       "ovthresh": 0.6,
                                       "funnel": True,
                                      })
        trained_pigs_funnel.train()

    def inet_pigs(self, dt):
        """
        Trained on the pig data
        """
        session = f"{dt}2"
        logging.info(f"[{session}] Training on Pigs VOC")
        trained_pigs = Trainer({"dataset": "pigs_voc",
                                  "net": "res101",
                                  "max_epochs": self.epochs * 2,
                                  "disp_interval": 100,
                                  "batch_size": self.bs,
                                  "cuda": True,
                                  "mGPUs": self.mGPUs,
                                  "session": session,
                                  "log_path": f"logs/pigs_voc_{session}",
                                  "is_optimising": True,
                                  # "and_test": True,
                                  "ovthresh": 0.6,
                                 })
        trained_pigs.train()

    def inet_pascal(self, dt):
        """
        Train on the pascal using classagnostic
        """
        session = f"{dt}3"
        logging.info(f"[{session}] Training on Pascal VOC Class Agnostic")
        trained_pascal_agnostic = Trainer({"dataset": "pascal_pigs",
                                           "net": "res101",
                                           "max_epochs": self.epochs,
                                           "disp_interval": 100,
                                           "batch_size": self.bs,
                                           "cuda": True,
                                           "mGPUs": self.mGPUs,
                                           "session": session,
                                           "class_agnostic": True,
                                           "log_path":
                                              f"logs/pascal_agnostic_{session}",
                                           "is_optimising": True,
                                           # "and_test": True,
                                           "ovthresh": 0.6,
                                          })
        trained_pascal_agnostic.train()

    def run(self):
        """
        Train 3 models:
        - ImageNet/PascalVOC/PigsVOC
        - ImageNet/PigsVOC
        - ImageNet/PascalVOC (class agnostic)
        """
        dt = self.get_dt()

        # Pascal then transfer to pigs
        self.inet_pascal_pigs(dt)
        self.slack.notify(text="Pascal transfer to Pig done", username=HOSTNAME)
        # Pascal then funnel to pigs
        self.inet_pascal_pigs_funnel(dt)
        self.slack.notify(text="Pascal funnel to Pig done", username=HOSTNAME)
        # Pigs only
        self.inet_pigs(dt)
        self.slack.notify(text="Pig testing done", username=HOSTNAME)
        # Pascal Class Agnostic
        self.inet_pascal(dt)
        self.slack.notify(text="Train Variants done!", username=HOSTNAME)


if __name__ == "__main__":
    # import coloredlogs
    # coloredlogs.install(level="DEBUG",
                        # fmt="%(asctime)s %(levelname)s %(module)s" + \
                            # "- %(funcName)s: %(message)s",
                        # datefmt="%Y-%m-%d %H:%M:%S")

    log_fn = f"./logs/{str(datetime.now()).replace(' ', '_')}_variants.log"
    logging.basicConfig(filename=log_fn,
                        level=logging.DEBUG,
                        format="%(asctime)s %(levelname)s %(module)s" +
                        "- %(funcName)s: %(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")

    TrainVariants(epochs=15, bs=8).run()
