from pandas import read_csv
import matplotlib as mpl
font = {'size'   : 16}
mpl.rc('font', **font)
from matplotlib import pyplot as plt


with open("./logs/pigs_voc_1810262/performance.csv", "r") as f:
    pigs = read_csv(f)

with open("./logs/pascal_pigs_voc_funnel_1810264/performance.csv", "r") as f:
    pascal_pigs_funnel = read_csv(f)

with open("./logs/pascal_pigs_voc_1810261/performance.csv", "r") as f:
    pascal_pigs = read_csv(f)

pascal_pigs_funnel["epoch"] = list(range(1,16))
pascal_pigs["epoch"] = list(range(1,16))

pigs = pigs[pigs.epoch <= 10].sort_values(by="epoch")
pascal_pigs = pascal_pigs[pascal_pigs.epoch <= 10]
pascal_pigs_funnel = pascal_pigs_funnel[pascal_pigs_funnel <= 10]

fig = plt.figure(figsize=(10,10))

ax = fig.add_subplot(2,1,1)
ax.plot(pascal_pigs.epoch, pascal_pigs.score,
        label="VOC 2007 + Pigs (modified)", c="xkcd:medium green",
        linewidth=2)
ax.plot(pascal_pigs_funnel.epoch, pascal_pigs_funnel.score,
        label="VOC 2007 + Pigs (added)", c="xkcd:pale red",
        linewidth=2)
ax.plot(pigs.epoch, pigs.score, label="Pigs only", c="xkcd:denim blue",
        linewidth=2)
ax.legend()
ax.set_xlabel("Epoch")
ax.set_ylabel("Mean Average Precision (mAP)")

ax = fig.add_subplot(2,1,2)

ax.plot(pascal_pigs.epoch, pascal_pigs.score,
        label="VOC 2007 + Pigs (modified)", c="xkcd:medium green",
        linewidth=2)
ax.plot(pascal_pigs_funnel.epoch, pascal_pigs_funnel.score,
        label="VOC 2007 + Pigs (added)", c="xkcd:pale red",
        linewidth=2)
ax.plot(pigs.epoch, pigs.score, label="Pigs only", c="xkcd:denim blue",
        linewidth=2)
ax.set_ylim([0.86,0.91])
ax.legend()
ax.set_xlabel("Epoch")
ax.set_ylabel("Mean Average Precision (mAP)")

plt.savefig("results.png")
plt.show()
