import logging
import json

from trainval_net import Trainer

EPOCHS = 20
NET = "res101"
DATASET = "pigs_voc"
BATCH_SIZE = 8
OVTHRESH = 0.6
RESUME_DATASET = "pascal_voc"
CHECK_SESSION = 1
CHECK_EPOCH = 15
CHECK_POINT = 1251
RESUME_CLASSES = 21
OPTIMISING = True
FUNNEL = True


def main():
    session = 18121
    lrs = [1e-3, 1e-4, 1e-5, 1e-6]
    momentums = [0.3, 0.5, 0.7]
    decay_steps = [3,5,7]
    decay_gammas = [1e-1, 1e-2, 1e-3]

    results = {}

    for lr in lrs:
        for decay_step in decay_steps:
            for decay_gamma in decay_gammas:
                for momentum in momentums:
                    params = {"optimizer": "sgd",
                              "lr": lr,
                              "decay_step": decay_step,
                              "decay_gamma": decay_gamma,
                              "momentum": momentum,
                              "max_epochs": EPOCHS,
                              "net": NET,
                              "dataset": DATASET,
                              "batch_size": BATCH_SIZE,
                              "session": session,
                              "ovthresh": OVTHRESH,
                              "resume_dataset": RESUME_DATASET,
                              "checksession": CHECK_SESSION,
                              "checkepoch": CHECK_EPOCH,
                              "checkpoint": CHECK_POINT,
                              "resume_classes": RESUME_CLASSES,
                              "optimising": OPTIMISING,
                              "cuda": True,
                              "transfer": True,
                              "resume": True,
                              "funnel": FUNNEL,
                              "verbose": True}
                    t = Trainer(params)
                    res = t.train()
                    results[str(session)] = res

                    with open("./output/res101/pigs_voc/faster_rcnn_10/"+\
                              "grid_search_results.json", "w") as f:
                        json.dump(results, f)

                    session += 1

def session_param_map():
    session = 18121
    lrs = [1e-3, 1e-4, 1e-5, 1e-6]
    momentums = [0.3, 0.5, 0.7]
    decay_steps = [3,5,7]
    decay_gammas = [1e-1, 1e-2, 1e-3]

    session_param = {}

    for lr in lrs:
        for decay_step in decay_steps:
            for decay_gamma in decay_gammas:
                for momentum in momentums:
                    session_param[session] = {"lr": lr,
                                              "decay_step": decay_step,
                                              "decay_gamma": decay_gamma,
                                              "momentum": momentum}
                    session += 1
    return session_param


if __name__ == "__main__":
    logging.basicConfig(filename="./output/res101/pigs_voc/faster_rcnn_10/"+\
                                 "grid_search.log",
                        level=logging.DEBUG,
                        format="%(asctime)s %(levelname)s %(module)s" +
                        "- %(funcName)s: %(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")
    main()
